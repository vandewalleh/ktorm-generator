package be.vandewalleh

class TableGenerator {
    operator fun invoke(table: Table): String {

        val (tableName, entityName, columns) = table

        val columnTemplate: (Column) -> String = { (name, type, _, primaryKey, reference) ->
            buildString {
                append("val ${name.value} = ${type.ktormType}(\"${name.value}\")")
                if (primaryKey) append(".primaryKey()")
                if (reference != null)
                    append(".references(${reference.tableName.value}) { it.${reference.referenceName.value} }")
                else
                    append(".bindTo { it.${name.value} }")
            }
        }

        val entityColumnTemplate: (Column) -> String = { (name, type, nullable, _, reference) ->
            buildString {
                if (reference != null) append("var ${reference.referenceName.value}: ${reference.entityName.value}")
                else append("var ${name.value}: ${type.kotlinType}")

                if (nullable) append("?")
            }
        }

        return buildString {
            append("internal open class ${tableName.value}(alias: String?) ")
            append(": Table<${entityName.value}>(\"${tableName.value}\", alias) {\n")
            append("    companion object : ${tableName.value}(null)\n\n")
            append("    override fun aliased(alias: String) = ${tableName.value}(alias)\n\n")
            append("    ${columns.joinToString("\n    ") { columnTemplate(it) }}\n")
            append("}\n\n")
            append("internal interface ${entityName.value} : Entity<${entityName.value}> {\n")
            append("    companion object : Entity.Factory<${entityName.value}>()\n\n")
            append("    ${columns.joinToString("\n    ") { entityColumnTemplate(it) }}\n")
            append("}\n")
        }
    }
}
