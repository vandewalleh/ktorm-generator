package be.vandewalleh

/**
 @see https://ktorm.liuwj.me/en/schema-definition.html
 */
@Suppress("unused")
enum class ColumnType(val ktormType: String, val kotlinType: String, val sqlType: String) {
    BOOLEAN("boolean", "Boolean", "boolean"),
    INT("int", "Int", "int"),
    SHORT("short", "Short", "smallint"),
    LONG("long", "Long", "bigint"),
    FLOAT("float", "Float", "float"),
    DOUBLE("double", "Double", "double"),
    DECIMAL("decimal", "BigDecimal", "decimal"),
    VARCHAR("varchar", "String", "varchar"),
    TEXT("text", "String", "text"),
    BLOB("blob", "ByteArray", "blob"),
    BYTES("bytes", "ByteArray", "bytes"),
    JDBC_TIMESTAMP("jdbcTimestamp", "Timestamp", "timestamp"),
    JDBC_DATE("jdbcDate", "Date", "date"),
    JDBC_TIME("jdbcTime", "Time", "time"),
    TIMESTAMP("timestamp", "Instant", "timestamp"),
    DATETIME("datetime", "LocalDateTime", "datetime"),
    DATE("date", "LocalDate", "date"),
    TIME("time", "Time", "time"),
    MONTHDAY("monthDay", "MonthDay", "varchar"),
    YEARMONTH("yearMonth", "YearMonth", "varchar"),
    YEAR("year", "Year", "int"),
    ENUM("enum", "Enum", "enum"),
    UUID("uuid", "UUID", "uuid"),
}
