package be.vandewalleh

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.UsageError
import java.io.File

class GeneratorCommand(private val generator: TableGenerator) : CliktCommand() {
    private fun promptReference(): Reference? {
        if (!(prompt("Reference", default = "false") { it.toBoolean() } ?: return null)) return null

        val tableName = prompt("Table name") { TableName(it) } ?: return null
        val referenceName = prompt("Reference Name") { ReferenceName(it) } ?: return null
        val entityName = prompt("Entity Name") { EntityName(it) } ?: return null

        return Reference(tableName, referenceName, entityName)
    }

    private fun promptColumn(): Column? {
        val name = prompt("Column name") { ColumnName(it) } ?: return null
        println(ColumnType.values().joinToString("|") { it.name.toLowerCase() })

        val type = prompt("Column type") { input ->
            enumValues<ColumnType>().find { it.name.equals(input, ignoreCase = true) }
        } ?: return null

        val nullable = prompt("Nullable", default = "false") { it.toBoolean() } ?: return null
        val primaryKey = prompt("Primary key", default = "false") { it.toBoolean() } ?: return null
        val reference = promptReference()

        return Column(name, type, nullable, primaryKey, reference)
    }

    override fun run() {
        val tableName = prompt("Table name") { TableName(it) } ?: throw UsageError("Table Name Required")

        val defaultEntityName = when {
            tableName.value.endsWith("s") -> tableName.value.removeSuffix("s") + "Entity"
            tableName.value.endsWith("Table") -> tableName.value.removeSuffix("Table") + "Entity"
            else -> null
        }

        val entityName = prompt("Entity name", default = defaultEntityName) {
            EntityName(it)
        } ?: throw UsageError("Entity Name Required")

        val columns = generateSequence {
            println()
            promptColumn()
        }.toList()

        val generated = generator(Table(tableName, entityName, columns))

        val fileName = tableName.value + ".kt"

        File(fileName).writeText(generated)

        println("\nGenerated output in $fileName")
    }
}
