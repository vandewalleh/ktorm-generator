package be.vandewalleh

data class Table(val name: TableName, val entityName: EntityName, val columns: List<Column>)

data class Column(
    val name: ColumnName,
    val type: ColumnType,
    val nullable: Boolean,
    val primaryKey: Boolean,
    val reference: Reference? = null,
)

data class Reference(
    val tableName: TableName,
    val referenceName: ReferenceName,
    val entityName: EntityName,
)

inline class EntityName(val value: String)
inline class ColumnName(val value: String)
inline class ReferenceName(val value: String)
inline class TableName(val value: String)

fun main(args: Array<String>) {
    val generator = TableGenerator()
    GeneratorCommand(generator).main(args)
}
