package be.vandewalleh

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class TableGeneratorTest {

    private val generator = TableGenerator()

    @Test
    fun generateUsers() {
        val res = generator(
            Table(
                TableName("Users"), EntityName("UserEntity"),
                listOf(
                    Column(name = ColumnName("id"), type = ColumnType.INT, nullable = false, primaryKey = true),
                    Column(
                        name = ColumnName("username"), type = ColumnType.VARCHAR,
                        nullable = false, primaryKey = false
                    ),
                    Column(
                        name = ColumnName("password"), type = ColumnType.VARCHAR,
                        nullable = true, primaryKey = false
                    ),
                )
            )
        )

        val expected = """
            internal open class Users(alias: String?) : Table<UserEntity>("Users", alias) {
                companion object : Users(null)

                override fun aliased(alias: String) = Users(alias)

                val id = int("id").primaryKey().bindTo { it.id }
                val username = varchar("username").bindTo { it.username }
                val password = varchar("password").bindTo { it.password }
            }

            internal interface UserEntity : Entity<UserEntity> {
                companion object : Entity.Factory<UserEntity>()

                var id: Int
                var username: String
                var password: String?
            }

        """.trimIndent()

        assertThat(res).isEqualTo(expected)
    }

    @Test
    fun generateNotes() {
        val res = generator(
            Table(
                TableName("Notes"), EntityName("NoteEntity"),
                listOf(
                    Column(name = ColumnName("uuid"), type = ColumnType.UUID, nullable = false, primaryKey = true),
                    Column(name = ColumnName("title"), type = ColumnType.VARCHAR, nullable = false, primaryKey = false),
                    Column(name = ColumnName("markdown"), type = ColumnType.TEXT, nullable = false, primaryKey = false),
                    Column(name = ColumnName("html"), type = ColumnType.TEXT, nullable = false, primaryKey = false),
                    Column(
                        name = ColumnName("user_id"), type = ColumnType.INT, nullable = false, primaryKey = false,
                        reference = Reference(TableName("Users"), ReferenceName("user"), EntityName("User"))
                    ),
                    Column(
                        name = ColumnName("updated_at"), type = ColumnType.DATETIME,
                        nullable = false, primaryKey = false
                    ),
                    Column(
                        name = ColumnName("updated_at"), type = ColumnType.DATETIME,
                        nullable = false, primaryKey = false
                    ),
                    Column(
                        name = ColumnName("deleted"), type = ColumnType.BOOLEAN,
                        nullable = false, primaryKey = false
                    ),
                    Column(
                        name = ColumnName("public"), type = ColumnType.BOOLEAN,
                        nullable = false, primaryKey = false
                    ),
                )
            )
        )

        val expected = """
            internal open class Notes(alias: String?) : Table<NoteEntity>("Notes", alias) {
                companion object : Notes(null)

                override fun aliased(alias: String) = Notes(alias)

                val uuid = uuid("uuid").primaryKey().bindTo { it.uuid }
                val title = varchar("title").bindTo { it.title }
                val markdown = text("markdown").bindTo { it.markdown }
                val html = text("html").bindTo { it.html }
                val user_id = int("user_id").references(Users) { it.user }
                val updated_at = datetime("updated_at").bindTo { it.updated_at }
                val updated_at = datetime("updated_at").bindTo { it.updated_at }
                val deleted = boolean("deleted").bindTo { it.deleted }
                val public = boolean("public").bindTo { it.public }
            }

            internal interface NoteEntity : Entity<NoteEntity> {
                companion object : Entity.Factory<NoteEntity>()

                var uuid: UUID
                var title: String
                var markdown: String
                var html: String
                var user: User
                var updated_at: LocalDateTime
                var updated_at: LocalDateTime
                var deleted: Boolean
                var public: Boolean
            }

        """.trimIndent()

        assertThat(res).isEqualTo(expected)
    }
}
