version = 1.0-SNAPSHOT
jar = target/ktormgenerator-$(version).jar
native-output = target/ktormgenerator-$(version)-linux-static

.PHONY = all

all: $(native-output)

$(native-output): $(jar)
	native-image --static --no-fallback --no-server -jar $< $@

$(jar):
	mvn package
